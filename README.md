# chatter

![Chatter](https://github.com/rubencarneiro/devices_images/blob/master/chatter.png "Chatter")

An IRC client for Ubuntu devices created by Robert Ancell ([Original repository](https://launchpad.net/chatter)).

## Building

Make sure to have
[Clickable](https://clickable.bhdouglass.com/en/latest/install.html) installed.

Depending on your target architecture, build the app with one of:

    clickable build --arch armhf
    clickable build --arch arm64
    clickable build --arch amd64

## Debugging

To run in Desktop mode run:

    clickable desktop

See [Clickable documenation](https://clickable-ut.dev/en/latest/debugging.html)
for on-device debugging and other options.

## Installing

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/chatter.ruben-carneiro)

Or depending on your target architecture run one of:

    clickable --arch armhf
    clickable --arch arm64
    clickable --arch amd64

